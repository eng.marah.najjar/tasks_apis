<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::factory()->create([
            'name' => 'category5',
            'email' => "admin@gmail.com",
            'password' => Hash::make('123456'),
            'created_at' =>  Carbon::now()->timestamp,
        ]);
    }
}
