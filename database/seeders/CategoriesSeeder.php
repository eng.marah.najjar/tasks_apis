<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Category::factory()->create([
            'name' => 'category1',
            'color' => "red"
        ]);
        Category::factory()->create([
            'name' => 'category2',
            'color' => "yallow"
        ]);
        Category::factory()->create([
            'name' => 'category3',
            'color' => "green"
        ]);
        Category::factory()->create([
            'name' => 'category4',
            'color' => "pink"
        ]);
        Category::factory()->create([
            'name' => 'category5',
            'color' => "blue"
        ]);
    }
}
