<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Category;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class Task extends Model
{
    use HasFactory;
    protected $table ='tasks';

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    public function getTaskById($id) {
        $result = Task::where('id',$id)->with('categories')->first();
        if($result)
            return $result;
        return NULL;
    }
    public function getTasks($description, $category_id, $deadline, $is_end, $page, $limit) {
    $query = Task::query();
    if($description != NULL)
        $query->where('tasks.description', 'LIKE', '%'.$description .'%');

    if($category_id != NULL)
        $query->whereHas('categories', function ($query) use ($categoryId) {
            $query->where('categories.id', $categoryId);
        });
    if($deadline != NULL)
        $query->where('tasks.deadline' , $deadline);  
    if($is_end != NULL)
        $query->where('tasks.is_end', $is_end);
    
    $query->orderBy('id','desc');
    $query->with('categories');

    return $query->paginate($limit)->onEachSide($page);
    }
    public function getExpiredtasks() {
        $result= Task::where('is_end' , 0)->where('deadline' , '<=' ,Carbon::now())->get();
        if($result)
            return $result;
        return [];
    }

}
