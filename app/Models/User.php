<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory;
    protected $guard_name = 'web';
    public function getUserByEmail($email) {
        $query = User::where('email', $email)->first();
        if($query)
            return $query;
        return false;
    }
}
