<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }
       /**
     * @OA\Get(
     * path="/api/getCatgeries",
     * summary="get All Catgeries",
     * description="get All Catgeries",
     * security={{"passport": {}}},
     * tags={"Category"},
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, validation error. Please try again")
     *        )
     *     )
     * )
     *
     */
    public function index()
    {
        //
        return $this->apiResponse([
            'success' => true,
            'message' => '',
            'result' => Category::all()
        ], 200);
    }
   
}
