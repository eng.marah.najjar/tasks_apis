<?php

namespace App\Http\Controllers\Apis;

use App\Models\Task;
use Illuminate\Http\Request;

use App\Http\Requests\createTaskRequest;

use App\Http\Controllers\Controller;


class TasksController extends Controller
{
    
    public $model;

    public function __construct(Task $model)
    {
        $this->model = $model;
    }
      /**
     * @OA\Post(
     * path="/api/getTasks",
     * summary="get All Tasks",
     * description="get All Tasks",
     * security={{"passport": {}}},
     * tags={"Tasks"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       @OA\Property(property="description", type="string", example="task 1"),
     *       @OA\Property(property="category_id", type="number", example="1"),
     *       @OA\Property(property="deadline", type="date", example="12-3-2023"),
     *       @OA\Property(property="is_end", type="boolean", example="1"),
     *       @OA\Property(property="page", type="number", example="1"),
     *       @OA\Property(property="limit", type="number", example="10"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong entries. Please try again")
     *        )
     *     )
     * )
     */
    public function index(Request $request)
    {
        //
        
        $description = (isset($request->description)) ? $request->description : null;
        $category_id = (isset($request->category_id)) ? $request->category_id : null;
        $deadline    = (isset($request->deadline)) ? $request->deadline : null;
        $is_end      = (isset($request->is_end)) ? $request->is_end : null;
        $page        = (isset($request->page) && is_int($request->page)) ? $request->page - 1 : 0;
        $limit       = (isset($request->limit) && is_int($request->limit)) ? $request->limit : 10;

        $tasks = $this->model->getTasks($description, $category_id, $deadline, $is_end, $page, $limit);

        return $this->apiResponse([
            'success' => true,
            'message' => '',
            'result' => $tasks
        ], 200);
    }

        /**
     * @OA\Post(
     * path="/api/createTask",
     * summary="create Task",
     * description="create Task",
     *  security={{"passport": {}}},
     * tags={"Tasks"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *      required={"description","categories","deadline","sub_tasks"},
     *       @OA\Property(property="description", type="text", example="task 1 Descriptions"),
     *       @OA\Property(property="categories", type="number", example="[1,2,3]"),
     *       @OA\Property(property="deadline", type="date", example="2023-3-12"),
     *       @OA\Property(property="sub_tasks", type="text", example="[sub task1 ,sub task2]"),
     *     
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong entries. Please try again")
     *        )
     *     )
     * )
     */
    public function createTask(createTaskRequest $request)
    {
        //
        $task = new Task();
        $task->description = $request->description;
        $task->deadline = $request->deadline;
        $task->sub_tasks = json_encode($request->sub_tasks);
        $task->deadline = $request->deadline;
        $task->save();

        $task->categories()->attach($request->categories);

        return $this->apiResponse([ 'success' => true, 'message' => 'create_task_successful' ,
        'result' => $task ], 200 );
    }

     /**
     * @OA\Get(
     * path="/api/getTaskById/{id}",
     * summary="get task details by id ",
     * description="get task details by id",
     *  security={{"passport": {}}},
     * tags={"Tasks"},
     *  @OA\Parameter(
     *    description="ID of task",
     *    in="path",
     *    name="id",
     *    required=true,
     *    example="1",
     *    @OA\Schema(
     *       type="integer",
     *       format="int64"
     *    )
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, validation error. Please try again")
     *        )
     *     )
     * )
     *
     */
    public function show($id)
    {
        $task = $this->model->getTaskById($id);
        if(!$task)
        return $this->apiResponse([ 'success' => false, 'message' => 'id_not_found'  ], 400 );
        return $this->apiResponse([ 'success' => true, 'message' => '' ,
        'result' => $task ], 200 );
    }
}
