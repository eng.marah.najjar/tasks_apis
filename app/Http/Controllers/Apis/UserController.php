<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserRecources;


use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    //
    public $model ;
    public $roleController;
    public $socialController;

    public function __construct(User $model) {
        $this->model = $model;
    }
      /**
     * @OA\Post(
     * path="/api/register",
     * summary="Sign up parent account",
     * description="Sign up parent account",
     * tags={"Auth"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass info",
     *    @OA\JsonContent(
     *       required={"email","name","password"},
     *       @OA\Property(property="email", type="string", format="email", example="admin@gmail.com"),
     *       @OA\Property(property="name", type="string", example=" name"),
     *       @OA\Property(property="password", type="string", format="password", example="123456"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, validation error. Please try again")
     *        )
     *     )
     * )
     *
     */
    public function register(RegisterRequest $request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        $tokenResult = $user->createToken('Laravel Password Grant Client');

        return $this->apiResponse([ 'success' => true, 'message' => 'create_account_successful' ,
        'result' => (object) [
            'user'=> new UserRecources($user) ,
            'access_token' => $tokenResult->accessToken, 
            'token_type'=>'Bearer' ] ], 200 );

    }
        /**
     * @OA\Post(
     * path="/api/login",
     * summary="Sign in",
     * description="Login by email, password",
     * tags={"Auth"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password"},
     *       @OA\Property(property="email", type="string", example="admin@gmail.com"),
     *       @OA\Property(property="password", type="string", format="password", example="123456"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */
    public function login(LoginRequest $request){
        $user = $this->model->getUserByEmail($request->email);
        if(!$user) 
            return $this->apiResponse([ 'success' => false , 'message' => 'error_email_or_password' ,'error_code' => 400], 400 );

        if(!Hash::check( $request->password , $user->password))
            return $this->apiResponse([ 'success' => false , 'message' => 'error_email_or_password' ,'error_code' => 400], 400 );

        $tokenResult = $user->createToken('Laravel Password Grant Client');
        return $this->apiResponse([ 'success' => true, 'message' => 'login_successful' ,
            'result' => (object) [
                'user'=> new UserRecources($user) ,
                'access_token' => $tokenResult->accessToken, 
                'token_type'=>'Bearer' ] ], 200 );
        
    }
}
