<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Task;

use App\Mail\expiredEmail;
use Illuminate\Support\Facades\Mail;
class SendDeadlineTasksEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:deadline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will close tasks expired and make it is_end & send emails as queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Task $task)
    {
        parent::__construct();
        $this->taskModel = $task;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $Expiredtasks = $this->taskModel->getExpiredtasks();

        foreach ($Expiredtasks as $task) {
            $task->is_end = 1;
            $task->save();
            Log::info('A new task has been expired');
            // TODO send email as queue
            // Queue the email for sending
            Mail::to('marah633@gmail.com')->queue(new expiredEmail($task));
        }

        if (sizeof($Expiredtasks) === 0) {
            $this->info('There are no tasks ready to launch');
        } else {
            $this->info('All tasks ready to be expired have been expired');
        }
    }
}
