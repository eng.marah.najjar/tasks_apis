<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'App\Http\Controllers\Apis\UserController@register');
Route::post('login', 'App\Http\Controllers\Apis\UserController@login');
Route::get('getCatgeries', 'App\Http\Controllers\Apis\CategoriesController@index');

Route::group(['middleware' => ['auth:api'] ], function ($api) {
    // Student order
    $api->post('createTask','App\Http\Controllers\Apis\TasksController@createTask');
    $api->post('getTasks','App\Http\Controllers\Apis\TasksController@index');
    $api->get('getTaskById/{id}','App\Http\Controllers\Apis\TasksController@show');

});